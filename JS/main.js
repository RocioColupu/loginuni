//JS PARA EL SIDEBAR

let listElements = document.querySelectorAll('.list__button--click'); //selecciona todo el elemento

listElements.forEach(listElement => {
        listElement.addEventListener('click', ()=>{
            listElement.classList.toggle('arrow'); //Agrega la clase arrow al elemento seleccionado antes
            
            let height = 0;
            let menu = listElement.nextElementSibling;
            console.log(menu.scrollHeight)
            if(menu.clientHeight == '0'){
                height=menu.scrollHeight;
            }
        menu.style.height = height+'px'
        })
});

const btnToggle = document.querySelector('.toggle-btn');
btnToggle.addEventListener('click', function(){
   document.getElementById('sidebar').classList.toggle('active') // Activa el sidebar y le agrega la clase active
    document.getElementById('container__principal').classList.toggle('move')
})


// JS PARA LOS IFRAMES CON LINK

var inicio = document.getElementById("titulo")
var miIframe = document.getElementById("mi-iframe");
var enlace2 = document.getElementById("Horario");
var enlace3 = document.getElementById("Docente");


inicio.addEventListener("click", function(){
    miIframe.src = "frame_principal.html";
    return false;
})

enlace2.addEventListener("click", function(){
    miIframe.src = "tabla.html";
    return false;
})

enlace3.addEventListener("click", function(){
    miIframe.src = "informaciondocente.html";
    return false;
})



//  JS PARA EL MENU DE LOS 3 PUNTOS
const iconoMenu = document.querySelector('#icono-menu');
    menu = document.querySelector('#menu2');

iconoMenu.addEventListener('click', function() {
    menu.classList.toggle('active');
})


// JS DOCENTE

const seleccionarFotobtn = document.querySelector("#seleccionarFoto");

seleccionarFotobtn.addEventListener("click", () => {
    const input = document.createElement("input");
    input.type = "file";
    input.accept = "image/*";
    input.onchange = (event) =>{
        const imagenSeleccionada = event.target.files[0];
    };
    input.onclick();
})

function mostrarImagen(imagen) {
    const reader = new FileReader();
    reader.readAsDataURL(imagen);
    reader.onload = () => {
      const contenedorImagen = document.querySelector("#contenedor-imagen");
      contenedorImagen.style.backgroundImage = `url(${reader.result})`;
    };
  }

input.onchange = (event) => {
    const imagenSeleccionada = event.target.files[0];
    mostrarImagen(imagenSeleccionada);
}; 